
## KDE Neon

### Installation

#### 1. Write image to SD card and boot it

Note: These instruction were written with image plasma-mobile-neon-20200228-073605.img.gz using Linux Mint

Download [latest image](https://images.plasma-mobile.org/pinephone/)

Write image to SD card using Etcher

Open the back cover of the phone, remove the battery (lift it up from the bottom side first) and insert the SD card into the slot in the back of the phone.  There are two slots, the one underneath is for the SIM card and the one on top of it is for the SD card.

Boot the phone and hopefully everything went right and you start seeing it boot to Linux.  Give it a minute and soon you will see the home screen with some apps.


#### 2. Resize the partition to use the entire SD card size (optional and only possible if you downloaded the image- building an image already sets the partitions to the max size)

Note that this step is completely optional and potentially could destroy the system you just booted to with so much excitement.  But it is best to do this **now** before you get any further just in case it fails.  If it does fail, then just reflash the SD card again from step 1 and skip this step.

The image copied to your SD card is only sized to 4 GB even if your SD card is larger.  To use all the space on your SD card follow these steps.  First open the Konsole app on the phone.  Then type in the command:

> $ sudo parted /dev/mmcblk0

This enters the parted utility, so you should see "(parted)" printed before your cursor.  Type in resizepart (don't type the "(parted)" it's just there to show you what the entire line should look like):

> (parted) resizepart


When it prompts you to enter the "Partition number" enter 1:

> Partition number? 1

It may warn you that the partition is being used.  Type y to continue:

> Yes/No? y

Next it will ask you for the new "End" of the partition.  Enter 100%:

> End? [3999MB]? 100%


Now type in quit:

> (parted) quit

Now one last command you need to run to finalize the size change.  Type in and run:

> $ sudo resize2fs /dev/mmcblk0p1

Wait a few minutes for it to complete.  Now you should have use of the entire SD card.  To check this you can run this command and look for the largest partition size listed which should match your SD card size:

> $ df -h

It's a little confusing looking at the output of that command on a small screen, but look for the section that has the /dev/mmcblk0p2 and look at the number next to that.


#### 3. Change the default password

TODO: I don't think this is a good idea yet.  It seems like if you change the password then the lock screen no longer accepts non digit passcodes.  And if you try to change the password to a different number from the command line the passwd command complains about the new number not being a strong enough password.  So skip this for now.


#### 4. Accessing the phone via SSH from a computer

TODO


#### 5. Hardening SSH

TODO


#### 6. Set timezone

If you downloaded the precompiled image you will need to set the timezone.  Use the following commands, substituting your appropriate time zone.

> $ sudo ln -s /usr/share/zoneinfo/America/New_York /etc/localtime


#### 7. Firewall setup

TODO

#### 8. Play some tunes

If you go into the Settings app then then "Audio" section you can change the playback destination between the headphones, internal earpiece (one speaker?) and the internal speaker (two speakers?)

You can run the speaker test program to test that they are working.  This will simply play white noise for a few seconds on each speakers.  For me the front left and right speakers worked but the rear did not. In the Konsole app run:

> $ speaker-test -c3

Let's play some music.  Install ffplay:

> $ sudo apt install ffmpeg

Now you will need to transfer some audio files to your phone.  You can do this any way you want, but one option is to use the sftp command from a computer.  If you have SSH already working this will also work.  Use the sftp command just like the ssh command:

> $ sftp phablet@device_ip

Use the 'lcd', 'lls' and 'put' commands to get your music files onto the phone.  Quit sftp and go back to a regular ssh session (or use Konsole on the phone) and use the ffplay command to play music like this:

> $ ffplay -nodisp '01 - Folsom Prison Blues (Live).mp3'

The vvave media player app on the phone opens and you can see your songs (if they are in the Music folder) and try to play them but I could not get any sound to work.  The app does grab album art from the internet which is cool!


<br>

### Notes

Lock PIN is 1234

To shutdown: in the Konsole run the command "sudo shutdown now"
 

<br>

### System- What works

Nice looking UI, better than other Plasma implimentations

Can install apps with Discover app

Has a dark mode for Plasma

Wifi connects

Scrolling in web browser (Angelfish) is fairly smooth, some display issues sometimes with all white/black screen

Cellular status with % shown on top bar

Brightness adjustment


### System- What doesn't work

Flashlight

Camera app

Can't set the date, time or timezone from the GUI

Keyboard starts in CAPS mode many times in apps

Phone app opens and you can try to dial a number but it immediately cancels the call

Once in the Settings app I could not close it to get back to the home screen


### Applications- What works

Psensor



### Applications- What doesn't work

Chromium runs but doesn't bring up a keyboard

Gnome Web runs but doesn't bring up a keyboard

Firefox runs but doesn't bring up a keyboard

Iceweasel crashes

Synaptic Package Manager

