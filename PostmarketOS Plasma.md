## PostmarketOS Plasma

### Installation

Download [latest image](http://images.postmarketos.org/pinephone/)

Write to SD card with Etcher


### System- What works

Wifi connects

Web browser (Angelfish) scrolls nicely

Cellular status in top bar

Screenshot from pull down menu


### System- What doesn't work

Enter button doesn't work on locked PIN screen (so locked out of phone)

Flickering screen (not seen in January image)

Can't install any apps yet?

Keyboard starts in CAPS mode

Phone app opens and you can try to dial but it immediately cancels the call

Camera

Flashlight
