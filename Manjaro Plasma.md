## Plasma Manjaro

### Installation

Download [latest image](https://osdn.net/projects/manjaro-arm/storage/pinephone/plasma-mobile/)

Write image to SD card with Etcher


### Notes

Pressing the power button once does a shutdown of the device.

Default username/pw: manjaro/manjaro


### System- What works

WiFi connects

Web browser works

I do see a cellular signal strength percentage and some cellular ID in the top status bar


### System- What doesn't work

Scrolling in browser is jerky

Phone app opens but if you try to place a call it says "Unable to make a call at this moment"

The pull down Shutdown button

The flashlight from the pull down

Brightness adjustment from the pull down

Settings -> Date and TIme: Changing timezone doesn't change time and the date picker doesn't work

