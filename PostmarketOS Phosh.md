## PostmarketOS Phosh

### Installation

#### 1. Write image to SD card and boot it

Note: These instruction were written with image pine-pinephone-20200228-phosh.img.xz

Download [latest image](http://images.postmarketos.org/pinephone/)

Write to SD card with Etcher

> Optional: Compile image from [wiki instructions](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)#Installation) and write to SD card.  If you do this these instructions may be slightly different than what is required on the built image.

Open the back cover of the phone, remove the battery (lift it up from the bottom side first) and insert the SD card into the slot in the back of the phone.  There are two slots, the one underneath is for the SIM card and the one on top of it is for the SD card.

Boot the phone and hopefully everything went right and you start seeing it boot to Linux.  Give it a minute and soon you will see the home screen with some apps.


#### 2. Resize the partition to use the entire SD card size (optional and only possible if you downloaded the image- building an image already sets the partitions to the max size)

Note that this step is completely optional and potentially could destroy the system you just booted to with so much excitement.  But it is best to do this **now** before you get any further just in case it fails.  If it does fail, then just reflash the SD card again from step 1 and skip this step.

The image copied to your SD card is only sized to 4 GB even if your SD card is larger.  To use all the space on your SD card follow these steps.  First open the Terminal app on the phone.  Then type in the command:

> $ sudo parted /dev/mmcblk0

This enters the parted utility, so you should see "(parted)" printed before your cursor.  Type in resizepart (don't type the "(parted)" it's just there to show you what the entire line should look like):

> (parted) resizepart


When it prompts you to enter the "Partition number" enter 2:

> Partition number? 2

It may warn you that the partition is being used.  Type y to continue:

> Yes/No? y

Next it will ask you for the new "End" of the partition.  Enter 100%:

> End? [3999MB]? 100%


Now type in quit:

> (parted) quit

Now one last command you need to run to finalize the size change.  Type in and run:

> $ sudo resize2fs /dev/mmcblk0p2

Wait a few minutes for it to complete.  Now you should have use of the entire SD card.  To check this you can run this command and look for the largest partition size listed which should match your SD card size:

> $ df -h

It's a little confusing looking at the output of that command on a small screen, but look for the section that has the /dev/mmcblk0p2 and look at the number next to that.


#### 3. Change the default password

For security purposes it is best to change the default password immediately.  To change this open the "Terminal" app.  Type in the following command:

> $ passwd

Then enter the current password (147147) and then type in the new password you wish to use twice. If successful it should say:

> passwd: password updated successfully


#### 4. Accessing the phone via SSH from a computer

TODO


#### 5. Hardening SSH

TODO


#### 6. Set timezone

If you downloaded the precompiled image you will need to set the timezone.  Use the following commands, substituting your appropriate time zone.

> $ sudo rm /etc/localtime

> $ sudo ln -s /usr/share/zoneinfo/America/New_York /etc/localtime


#### 7. Firewall setup

TODO


#### 8. Turn on the cellular modem (Work in progress- not working for me)

Run these commands from the Terminal:

> $ sudo apk add ofonoctl

> $ ip link set wwan0 up

> $ ofonoctl poweron

> $ ofonoctl online

> $ ofonoctl wan --connect --append-dns

Use this command to see some status:

> $ ofonoctl list


#### 9. Turn on the speakers, play some tunes

Run these commands from the Terminal:

> $ sudo apk add alsa-utils

> $ amixer sset Master unmute

> $ alsamixer -V all

alsamixer is a really cool program that shows a GUI in the Terminal to alter volume and input levels.  Press the "globe" key on virtual keyboard to change the language to "terminal" so you can now use the ">_" key to access the arrow and Esc keys.  Use left and right arrows to change between the "Master" and "Capture" devices.  Use the up and down arrows to change the volume level up and down.  Press the "Tab" key to switch between seeing only playback devices, capture devices or all devices.  Press "Esc" to exit the program.

You can run the speaker test program to test that they are working.  This will simply play white noise for a few seconds on each speakers.  For me the front left and right speakers worked but the rear did not.

> $ speaker-test -c3

Let's play some music.  Install ffplay:

> $ sudo apk add ffmpeg-libs ffplay

Now you will need to transfer some audio files to your phone.  You can do this any way you want, but one option is to use the sftp command from a computer.  If you have SSH already working this will also work.  Use the sftp command just like the ssh command:

> $ sftp demo@device_ip

Use the 'lcd', 'lls' and 'put' commands to get your music files onto the phone.  Quit sftp and go back to a regular ssh session (or use Terminal on the phone) and use the ffplay command to play music like this:

> $ ffplay -nodisp ./01\ -\ Folsom\ Prison\ Blues\ \(Live\).mp3

One thing I did notice is that if you are using ssh from a computer and are playing a song, altering the volume using the Terminal app does not change the volume of the playing song.  Each user session seems to have it's own separate control and settings for the volume of the speakers.  If when playing a song in an ssh session, if you try to play a song in Terminal it does not play, but waits until the ffplay command from the ssh session is closed.

I haven't been able to get headphones to work yet.  Plugging them in does nothing and music playing on the speakers continues to do so.

<br>

### Helpful commands to install via Terminal:

sudo apk add htop curl git


<br>

### Notes

Default PIN to unlock is 147147

In the Terminal app, you can hit the little globe icon and change the language to "terminal" which changes the "." button to a ">_" button.  Press this to get access to other keys like up, down, ESC, Tab.  There don't appear to be any way to use CTRL commands yet.

If you have a windows displaying that is too large for the screen and you can't dismiss it, hit the up arrow at the bottom of the screen to show the home screen.  The window shown in the section of active applications should be the one causing the problem, so simply press the "X" button to close that window then return back to the app.  I find this commonly happens in the Settings app.


### System- What works

Very nice UI

Pull down and press the button to shutdown

Connecting to wifi now works

Screen brightness adjustment via the pull down



### System- What doesn't work


Calls and SMS

Camera

Battery charging indicator


<br>

### Applications- What works

Midori web browser works

Maps


<br>

### Applications- What doesn't work

"Web" browser app only shows a white screen


