
## PureOS

### Installation

#### 1. Write image to SD card and boot it

Note: These instruction were written with image http://www.mozzwald.com/pp/pureos-pinephone-20200226.img.xz using Linux Mint

Download [latest image](https://wiki.pine64.org/index.php/PinePhone_Software_Release#PureOS)

Write image to SD card using Etcher

Open the back cover of the phone, remove the battery (lift it up from the bottom side first) and insert the SD card into the slot in the back of the phone.  There are two slots, the one underneath is for the SIM card and the one on top of it is for the SD card.

Boot the phone and hopefully everything went right and you start seeing it boot to Linux.  Give it a minute and soon you will see the lock screen (PIN 123456 to unlock) and the setup wizard starts.


#### 2. Resize the partition to use the entire SD card size

Note that this step is completely optional and potentially could destroy the system you just booted to with so much excitement.  But it is best to do this **now** before you get any further just in case it fails.  If it does fail, then just reflash the SD card again from step 1 and skip this step.

The image copied to your SD card is only sized to 4 GB even if your SD card is larger.  To use all the space on your SD card follow these steps.  First open the Konsole app on the phone.  Then type in the command:

> $ sudo parted /dev/mmcblk0

This enters the parted utility, so you should see "(parted)" printed before your cursor.  Type in resizepart (don't type the "(parted)" it's just there to show you what the entire line should look like):

> (parted) resizepart


When it prompts you to enter the "Partition number" enter 2:

> Partition number? 2

It may warn you that the partition is being used.  Type y to continue:

> Yes/No? y

Next it will ask you for the new "End" of the partition.  Enter 100%:

> End? [3999MB]? 100%


Now type in quit:

> (parted) quit

Now one last command you need to run to finalize the size change.  Type in and run:

> $ sudo resize2fs /dev/mmcblk0p2

Wait a few minutes for it to complete.  Now you should have use of the entire SD card.  To check this you can run this command and look for the largest partition size listed which should match your SD card size:

> $ df -h

It's a little confusing looking at the output of that command on a small screen, but look for the section that has the /dev/mmcblk0p2 and look at the number next to that.


### More quick notes (To clean up later)


I am able to initiate a phone call and answer on the other phone, but can't hear any audio.

To install an ssh server:

> $ sudo apt install openssh-server

Apps:

> $ sudo apt install midori htop gnome-core

Follow [these instructions](https://wp.puri.sm/posts/easy-librem-5-app-development-scale-the-screen/) to be able to adjust the screen size.  I found a scale of 1.3 to be good.  Unfortunately for scale values 1.34 to 1.99 the virtual keyboard also gets smaller. But at scale 1.33 the keyboard readjusts to normal size again.  You probably also need to install these packages via apt before building wlr-randr: git, pkg-config

