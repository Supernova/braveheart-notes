# braveheart-notes


These are some notes as I test the [PinePhone](https://wiki.pine64.org/index.php/PinePhone) BraveHeart edition.  Updates will be made as I am able to test these systems.

Currently I am only going to be testing Ubuntu Touch.  I have tried all the ones below and have found I like Touch the best.  Phosh is also very nice, especially on top of Debian but the interface is just too basic for me.  I like the various swipe actions of Touch and now it has OTA updates.


My current order of useability:

1. Manjaro Phosh- Getting closer to fully functional every week.
2. Mobian- Yes I like Phosh, and being Debian based I am very familiar with terminal commands.
3. Ubuntu Touch- I like the interface, but I don't like how the file system is now locked down.  It makes tweaking and installing apps much harder.  Most apps in the store seem like web apps which I don't prefer.  This would be a great phone though for someone who wants something that they don't tinker with.
4. PostMarketOS Phosh
5. PureOS
6. PostmarketOS Plasma- Not so much a fan of Plasma/KDE on the phone
7. Plasma/KDE Neon

Note: I removed my chart of currently working features for each system because changes are being made so fast I cannot keep up to test them all.

Where to get PinePhone news:

 - [Pine64 forum](https://forum.pine64.org/forumdisplay.php?fid=2)
 
 - [Telegram channel](https://t.me/PINE64_News)
 
 - Mastodon: [@PINE64@fosstodon.org](@PINE64@fosstodon.org)
 
 - [LINux on MOBile](http://linmob.net) blog
 
 