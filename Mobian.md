## Mobian

[https://wiki.mobian-project.org](https://wiki.mobian-project.org)

- Installation

Download the newest [image file](https://images.mobian-project.org/pinephone/nightly/)

Write it to SD card with [Etcher](https://www.balena.io/etcher/)

Set timezone in Settings -> Details -> Date & Time

### SSH

> sudo apt install openssh-server

> sudo systemctl start sshd

> ssh debian@192.168.1.22

password is: 1234


- Good

WiFi connects

I like the visual appeal of Phosh


- Bad

Web browser works but is a little slow (probably because the entire system CPU usage is high)

The Usage app is nice, shows system CPU usage.

Caution, the CPU gets very hot after a while!  I would be concerned about damage to the hardware.





sudo apt update
sudo apt upgrade

sudo apt install midori htop gnome-core

Follow [these instructions](https://wp.puri.sm/posts/easy-librem-5-app-development-scale-the-screen/) to be able to adjust the screen size.  I found a scale of 1.3 to be good.  Unfortunately for scale values 1.34 to 1.99 the virtual keyboard also gets smaller. But at scale 1.33 the keyboard readjusts to normal size again.  You probably also need to install these packages via apt before building wlr-randr: git, pkg-config
