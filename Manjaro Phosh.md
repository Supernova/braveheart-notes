## Manjaro Phosh

### Installation

Download [latest image](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/)

Write image to SD card with Etcher


### Notes


Default username/pw: manjaro/123456


### System- What works

WiFi connects

Web browser works

Cellular calling

SMS

Flashlight

Brightness adjustment


### System- What doesn't work

Timezone is not set in initial configuration steps.


### Working Applications

Dino (XMPP chat)

Firefox

Telegram


### Useful tweaks

[Add user folders](https://forum.pine64.org/showthread.php?tid=10991&pid=74888#pid74888) so that the camera can save photos taken into your home Pictures folder

Adjust the screen scaling:

- From the terminal edit usr/share/phosh/phoc.ini

- Change the "scale" value and save the file

- Run: sudo systemctl restart phosh
