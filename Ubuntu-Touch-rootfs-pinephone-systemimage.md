## Ubuntu Touch

![UT Home Screen](images/ut_home.jpg "Home Screen")

Note: These instruction are written for the daily builds of rootfs-pinephone-systemimage available at [https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/) which includes a new Over-The-Air update system.

Tested with build #66.

Instructions I wrote for the older rootfs-pinephone images are still available [here](https://codeberg.org/Supernova/braveheart-notes/src/branch/master/Ubuntu%20Touch.md) and may or may not work with this new image.


### Installation and Setup


<br>

#### 1. Write image to SD card and boot it

Download the [image file](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/) from UBPorts

Write it to an SD card with [Etcher](https://www.balena.io/etcher/)

Open the back cover of the phone, remove the battery (lift it up from the bottom side first) and insert the SD card into the slot in the back of the phone.  There are two slots, the one underneath is for the SIM card and the one on top of it is for the SD card.

Boot the phone and hopefully everything went right and you start seeing it boot to Linux.  Give it a minute and soon you will see some prompts for an initial setup.  The prompts will help you to choose your language, connect to your wifi, choose your city/timezone, and set a security PIN.


<br>

#### 2. (Work In Progress) Resize the partition to use the entire SD card size

The newest image creates a userdata partition of 8GB which is enough to do basic testing on the device.  I will need to experiment with this to determine if this partition can be expanded.  The new system image contains 10 partitions now.


<br>

#### 3. UT Tweak Tool


Screen size (scaling):

You can alter the size of icons and text using the UT Tweak Tool app.  Install this from the OpenStore.  Then open UT Tweak Tool, enter your PIN or password, select "Scaling" and use the slider the select a new value.  It will ask you to restart Unity 8 to apply the settings, and sometimes I have had problems with Unity 8 coming back with a "Restart", so you may need to do a full shut down instead.

Dark Mode:

Under the "Experimental"/"System theme" section you can choose between the default light theme and a dark theme.  The dark theme is still a little jarring in that apps start in the light mode then change to dark mode so there is a few seconds between the transition.

Restarting Unity8 is required, but the restart may get stuck at the Ubuntu Touch boot screen.  If your phone doesn't boot after several minutes you will need to hold the power button until the phone shuts off, then boot the phone again.

<br>

#### 4. OS Updates

OS Updates are performed from the Settings app.  Go to the "Updates" section near the bottom and it will search for any updates from the Ubuntu Touch repository.

You can switch to different branches of the OS to get either a stable release, release candidates for the stable branch (get new features earlier) or the development branch (live dangerously).  Changing the branch may reset any modifications you have made since the system partition is reinstalled for the new branch you select.  To change the branch run:

> sudo mount -o remount,rw /

Choose one of these:

> sudo system-image-cli --switch='16.04/arm64/mainline/rc' --progress=dots

> sudo system-image-cli --switch='16.04/arm64/mainline/devel' --progress=dots

> sudo system-image-cli --switch='16.04/arm64/mainline/stable' --progress=dots



<br>

#### 5. Accessing the phone via SSH from a computer

Using SSH to access a terminal session on your phone makes things so much easier than trying to type commands on the phone's keyboard.  There is an SSH server built in however to run it and access it requires modifying some settings and then starting the SSH server.  Here are the commands to do so:

> sudo mount -o remount,rw /

> cd /etc/init

> sudo umount ssh.override

> sudo mv ssh.override ssh.override_bak

> sudo mount -o remount,ro /

> sudo start ssh

Now try to log in to your phone from a computer on the same network.  You will need to find out the IP address of your phone first.  Run the command ‘ip a | grep wlan0’ in the phone’s Terminal app. You are looking for something like “inet 192.168.0.5”, the numbers are your phone's IP address.  From you computer connect to your phone like this, using your phone's IP address:

> ssh phablet@192.168.0.5

Once you have successfully logged in, it is recommended that you change your SSH port from the default.  To edit the config file run:

> sudo mount -o remount,rw /

> sudo nano /etc/ssh/sshd_config

Find the line that beings with "Port" and change the number to something else you will remember in the 1000 to 9999 range.  Hit CTRL-O and ENTER to save the changes, then CTRL-X to exit back to the terminal command.  Now lock the file system from editing again:

> sudo mount -o remount,ro /

> sudo restart ssh

You are still connected with the default port, so disconnect from ssh and try to use your new port to connect:

> exit

> ssh -p [ssh port] phablet@[phone ip]


<br>

#### 6. Installing software

There is an "OpenStore" app that can be used to install prepackaged applications for Ubuntu Touch.  However many are not working at this time so you will need to just try something to see if it works.

You can also install standard Debian packages, however this could possibly cause problems.  The filesystem is normally locked to prevent changes, so it is necessary to unlock the filesystem to install Debian packages.  Here are the commands to run in the Terminal to install packages:

> sudo mount -o remount,rw /

> sudo apt update

Install any packages you want now.  Finally relock the filesystem:

> sudo mount -o remount,ro /


<br>

### Notes

- Hold down the power button for a few seconds to get a shutdown/restart dialog.

- A list of [what is and is not working](https://gitlab.com/ubports/community-ports/pinephone) from the UBPorts GitLab

- [Interface Essentials](https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-just-another-phone-os-not-at-all-162)

- [Helpful tutorials](https://forums.ubports.com/topic/2008/the-ubports-tutorial-links-collection)

- Get apps from the [Open Store website](https://open-store.io) or built in app.  The nice thing about the website is it shows what is a webapp or native app.

<br>

### System- What works

- Very nice initial setup screens (language, timezone, phone name, PIN code and wifi connection)

- WiFi connects

- Default browser works very well, scrolling is smooth, the virtual keyboard works

- UI is very responsive and looks great, similar to the former Unity desktop environment

- Lots of options in the settings (which I haven't played with much)

- OpenStore has a good selection of apps to download.

- Brightness adjustment from the top menu

- Brightness adjustment in settings

- Auto screen rotation

<br>

### System- What doesn't work

- Battery usage is much better on recent releases.  However the battery level remaining that is displayed is not always accurate right after resuming from a sleep state.

- Phone dialing is still not completely functional yet.

- Bluetooth won't turn off


<br>

### Applications- What works

Native apps:

- Calculator
- Calendar
- Clock
- Contacts
- External Drives
- File Manager
- Gallery
- Morph Browser
- Notes
- Open Store
- System Settings
- Terminal
- UBPorts
- Weather

Third party apps:

- Circle Message
- Document Viewer
- Fluffy Chat (Matrix client)- though there does not seem to be encryption enabled yet
- [OnionBrowser](https://open-store.io/app/onion.nanuc.org)
- Pure Maps
- Stellarium- very cool star viewing app
- uMastonauts (Mastodon client)
- uNav
- UT Tweak Tool
- WireUT (Wire client)- The text is very small but it works well!  One annoyance is the virtual keyboard covers the text input area so you can't see the result of your typing.

Click Apps:

Install .click apps using the UT Tweak Tool (available from the Open Store).  Open UT Tweak Tool, type in your PIN/password, then press the menu at the top left corner of the app.  Select "System", then "Install click package".  Press the "Pick..." button and select the .click package you want to install.


<br>

### Applications- What doesn't work

Native apps:

- Camera
- Messaging
- Phone (opens but calls don't work)


Third party apps:

- Converse.js- I enter my login credentials (tried two accounts) but it will never connect.  But conversejs.org in the web browser does work.
- Feedly Web- I can log in but the text is so large and I can see the feed index but can't see the list of articles for the feeds.
- Movim
- TTRSS-Reader- crashes on start
- uWriter- crashes on startup

