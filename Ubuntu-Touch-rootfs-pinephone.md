## Ubuntu Touch

![UT Home Screen](images/ut_home.jpg "Home Screen")


Note: These instruction are written for the daily builds of rootfs-pinephone available at [https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/).  There is a newer experimental image available at [https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone-systemimage/) which includes a new Over-The-Air update system.  This is the image I will now be using, so this page will remain here only as reference for using the older daily builds.


### Installation and Setup


#### 1. Write image to SD card and boot it

Download the [image file](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/) from UBPorts

Write it to an SD card with [Etcher](https://www.balena.io/etcher/)

Open the back cover of the phone, remove the battery (lift it up from the bottom side first) and insert the SD card into the slot in the back of the phone.  There are two slots, the one underneath is for the SIM card and the one on top of it is for the SD card.

Boot the phone and hopefully everything went right and you start seeing it boot to Linux.  Give it a minute and soon you will see some prompts for an initial setup.  The prompts will help you to choose your language, connect to your wifi, choose your city (but your timezone may still be wrong after this), and choose a name for your phone.

Note: The default password is: phablet

[Interface Essentials](https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-just-another-phone-os-not-at-all-162)


#### 2. Resize the partition to use the entire SD card size (optional)

Note that this step is completely optional and potentially could destroy the system you just booted to with so much excitement.  But it is best to do this **now** before you get any further just in case it fails.  If it does fail, then just reflash the SD card again from step 1 and skip this step.

The image copied to your SD card is only sized to 4 GB even if your SD card is larger.  To use all the space on your SD card follow these steps.  First open the Terminal app on the phone.  Then type in the command:

> $ sudo parted /dev/mmcblk0

This enters the parted utility, so you should see "(parted)" printed before your cursor.  Type in resizepart (don't type the "(parted)" it's just there to show you what the entire line should look like):

> (parted) resizepart


When it prompts you to enter the "Partition number" enter 1:

> Partition number? 1

It may warn you that the partition is being used.  Type y to continue:

> Yes/No? y

Next it will ask you for the new "End" of the partition.  Enter 100%:

> End? [3999MB]? 100%


Now type in quit:

> (parted) quit

Now one last command you need to run to finalize the size change.  Type in and run:

> $ sudo resize2fs /dev/mmcblk0p1

Now you should have use of the entire SD card.  To check this you can run this command and look for the largest partition size listed which should match your SD card size:

> $ df

It's a little confusing looking at the output of that command on a small screen, but look for the section that has the /dev/mmcblk0p1 and look at the number next to that.


#### 3. Change the default password

Changing the PIN is now a part of the initial setup wizard (very nice)!  But if you on an older version here are the previous instructions:

> For security purposes it is best to change the default password immediately.  It also makes unlocking the phone easier if you choose a PIN number instead of a password.  Typing in 'phablet' and ENTER as the default password when unlocking the phone is prone to spelling errors.  If you change to a PIN the number pad has much bigger buttons so typing errors are less likely and you don't have to press ENTER at the end.

> To change to a PIN number, open the "System Settings" app.  Scroll down to "Security & Privacy", select that option, select "Locking and unlocking", then select "Lock security".  Select "4-digit passcode...", enter "phablet" as the existing passphrase and then enter your new PIN number as the new passcode.

If you want to use a passphrase instead of a PIN, the easiest way to do that if you are not familiar with using the Terminal app is to go into the Settings app, select "Security & Privacy", then "Locking and unlocking".  In the "Lock security" section select "Passphrase" to set a new character based password.


#### 4. Accessing the phone via SSH from a computer

Did you know that Ubuntu Touch comes with an SSH server already enabled and running when first setup?  This is good in one way in that it makes it very easy to SSH into the phone and start using the command line to manage the phone just like a Linux computer.  This is also bad because **it is very easy for anyone else to also SSH into your new phone**.  If you followed Step 2 above to change the default password you have already taken steps to prevent anyone from accessing your phone using the well known default user credentials.

The phone has a Terminal application but it is a bit harder to use without easy access to CTRL keys and arrows in the virtual keyboard.  For easier terminal usage, connect to your phone via SSH from a computer on the same network that your phone is on.  The username for the phone is "phablet" so connect via ssh with the command:

> $ ssh phablet@phone_ip

The phone_ip above is the ip address of the phone, which you can get by running the command 'ip a show dev wlan0 | grep inet' in the phone's Terminal app.  You are looking for something like "inet 192.168.0.2".  Use the IP address like this:

> $ ssh phablet@192.168.0.2

You may be prompted to accept the certificate of the phone so do this.  Then enter the password or PIN number you setup from step 2.  If all is well you will now be connected to the phone.

Here is [an excellent overview of the file structure by Rob Braxman](https://www.youtube.com/watch?v=rMmWmNyDKG8&feature=youtu.be)


#### 5. Hardening SSH

This is very important to do since you are now walking around with essentially a full computer in your pocket.  Doing at the very least step number 3 (changing the default password) will help prevent unauthorized access to your phone.  This step will also help protect your device.  We want to make it less obvious that your phone is running an SSH server and disable the server altogether when you don't need it.

***A. Change the default ssh server port***

In Terminal (or using ssh from another PC) run:

> $ sudo nano /etc/ssh/sshd_config

Change the port from the default.  Modify "Port 22" to something like "Port 7576".  Hit CTRL-O and ENTER to save the changes and then hit CTRL-X to exit nano.

Now restart the ssh server for the changes to be applied:

> $ sudo service ssh restart

If you are connected by ssh into the phone from another PC then exit and try to ssh in again to verify the new port is active by using this new ssh command which specifies the new port to use:

> $ ssh -p 7576 phablet@phone_ip

***B. Disable SSH when not needed***

If you don't need to use SSH then you should disable it (so it doesn't start automatically on the next phone restart) and turn it off.

> $ sudo touch /etc/ssh/sshd_not_to_be_run
> 
> $ sudo service ssh stop

To enable SSH again run:

> $ sudo rm /etc/ssh/sshd_not_to_be_run
> 
> $ sudo service ssh start

These commands are a bit long via the Terminal virtual keyboard so you can use [my scripts](https://codeberg.org/Supernova/utx) enssh.sh and disssh.sh

***C. Use a certificate for ssh logins and disable normal password authentication***

TODO


#### 6. Set timezone

Setting the timezone in the Settings app doesn't work so you need to do it in the Terminal.  To see a list of available timezones type in the following command but don't hit ENTER to run it.  Instead hit TAB (or simply touch the screen in the phone's Terminal app) and you can see a list of all time zones.  You may need to press tab twice to invoke the autocomplete feature.

> $ sudo timedatectl set-timezone

When you find the appropriate time zone enter it like this example:

> $ sudo timedatectl set-timezone America/New_York

Sometimes that may not work, and if that is the case try:

> $ sudo dpkg-reconfigure tzdata

And choose your timezone.


#### 7. Firewall setup

Using a firewall will also help protect your phone from unauthorized use.  Right now we only need access to SSH so you can enable the ufw firewall with these commands:

> $ sudo ufw default deny incoming

> $ sudo ufw default allow outgoing

> $ sudo ufw allow 7576/tcp

> $ sudo ufw enable


#### 8. Turn on the speakers, play some tunes

Run these commands from the Terminal to turn the speakers on at 25% max volume:

> $ sudo modprobe snd_soc_simple_amplifier

> $ sudo modprobe snd_soc_simple_card_utils

> $ amixer -c 0 set 'DAC' unmute

> $ amixer sset 'Line Out' 25%

> $ alsamixer -V all

alsamixer is a really cool program that shows a GUI in the Terminal to alter volume and input levels.  You need to change the top row of keys to "Scroll Keys" to use the arrows, and to "Function Keys" to use Esc.  Use left and right arrows to change between the devices.  Use the up and down arrows to change the volume level up and down.  Press the "Tab" key to switch between seeing only playback devices, capture devices or all devices.  Press "Esc" to exit the program. Using ssh from a computer is easier when using this program.

You can run the speaker test program to test that they are working.  This will simply play white noise for a few seconds on each speakers.  For me the front left and right speakers worked but the rear did not. In the Terminal app run:

> $ speaker-test -c3

Let's play some music.  Install ffplay:

> $ sudo apt install ffmpeg

Now you will need to transfer some audio files to your phone.  You can do this any way you want, but one option is to use the sftp command from a computer.  If you have SSH already working this will also work.  Use the sftp command just like the ssh command:

> $ sftp phablet@device_ip

Use the 'lcd', 'lls' and 'put' commands to get your music files onto the phone.  Quit sftp and go back to a regular ssh session (or use Terminal on the phone) and use the ffplay command to play music like this:

> $ ffplay -nodisp '01 - Folsom Prison Blues (Live).mp3'


#### 9. UT Tweak Tool


Screen size (scaling):

You can alter the size of icons and text using the UT Tweak Tool app.  Install this from the OpenStore.  Then open UT Tweak Tool, enter your PIN or password, select "Scaling" and use the slider the select a new value.

Dark Mode:

Under the "Experimental"/"System theme" section yiu can choose between the default light theme and a dark theme.  It's still a little jarring in that apps start in the light mode then change to dark mode so there is a few seconds between the transition.


#### 10. Powersave mode

The CPU can be put into some different [power usage modes](https://wiki.debian.org/CpuFrequencyScaling).  You can view the current mode by viewing the contents of this file:

> $ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

You can alter this file to change the power mode.  This does need to be done as root.  There are several possible values that can be set, view them with:

> $ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors

Then set one of the values:

> $ sudo su

> $ echo "powersave" > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor

So until there is better power management you can use "powersave" to potentially extend battery runtime.


> $ sudo apt install sysfsutils

This tool may be able to automatically set a specific power mode, more testing needed.


#### 11. Play!

That's all I have for basic setup steps for now, so go and have fun!  For example [play with the LED](https://codeberg.org/Supernova/utx/src/branch/master/ledfun.sh).

[Other tutorials](https://forums.ubports.com/topic/2008/the-ubports-tutorial-links-collection) from UBPorts

Pine64 [PinePhone Forum](https://forum.pine64.org/forumdisplay.php?fid=125)

[UBPorts Gitlab Issues](https://gitlab.com/ubports/community-ports/pinephone/issues)

[UBPorts Forum](https://forums.ubports.com/recent)


<br>

### Helpful commands to install via Terminal:

sudo apt install curl git

I have not had a chance to try these commands yet, but [some commands to enable the sound and modem](https://forum.pine64.org/showthread.php?tid=8923)

I am keeping a repository of [Ubuntu Touch shell scripts](https://codeberg.org/Supernova/utx) for the PinePhone.


<br>

### Notes

- Hold down the power button for a few seconds to get a shutdown/restart dialog.

- A list of [what is and is not working](https://gitlab.com/ubports/community-ports/pinephone) from the UBPorts GitLab

- The default password is "phablet".  If you want a 4 digit PIN to unlock the phone instead, open the Settings app, then select "Security & Privacy", then select "Locking and unlocking", then select "Lock security", then select "4-digit passcode", enter the current password (phablet if you haven't changed it) then choose a new PIN to use.  This will also make the PIN number the root (sudo) password.

- [Helpful tutorials](https://forums.ubports.com/topic/2008/the-ubports-tutorial-links-collection)

- Get apps from the [Open Store website](https://open-store.io) or built in app.  The nice thing about the website is it shows what is a webapp or native app.

<br>

### System- What works

- Very nice initial setup screens (language, timezone (which doesn't take), phone name and wifi connection)

- WiFi connects

- Default browser works very well, scrolling is smooth, the virtual keyboard works

- UI is very responsive and looks great, similar to the former Unity desktop environment

- Lots of options in the settings (which I haven't played with much)

- OpenStore has a good selection of apps to download.

- Brightness adjustment from the top menu

- Brightness adjustment in settings

<br>

### System- What doesn't work

- Battery power sometimes seems to discharge even when phone is powered off (for example I shut it down with 37% charge before bed and in the morning the battery was drained).  You can see this in the battery settings that has a chart of battery charge.  This does not always happen though, perhaps the phone does not always shut down completely.  For now I recommend removing the battery after a shutdown unless you are going to keep it plugged in to charge.

- In order for the phone to charge the power cable must be plugged in before powering up the phone.  If you disconnect the cable while powered on and reconnect it it will not start charging again, but requires a reboot.

- Changing the time and timezone from settings, but can do it from the Terminal [like this](https://github.com/dirkjanbuter/pinephone-commands/blob/master/set-timezone-amsterdam.sh)

- Couldn't detect a bluetooth keyboard in discover mode

- The phone app opens and you can dial a number and it tries to call but eventually doesn't complete.

<br>

### Applications- What works

Native apps:

- Calculator
- Calendar
- Clock
- Contacts
- External Drives
- File Manager
- Gallery
- Morph Browser
- Open Store
- Terminal
- Weather

Third party apps:

- Fluffy Chat (Matrix client)- though there does not seem to be encryption enabled yet
- [OnionBrowser](https://open-store.io/app/onion.nanuc.org)
- Stellarium- very cool star viewing app
- UT Tweak Tool
- WireUT (Wire client)- The text is very small but it works well!  One annoyance is the virtual keyboard covers the text input area so you can't see the result of your typing.

Click Apps:

Install .click apps using the UT Tweak Tool (available from the Open Store).  Open UT Tweak Tool, type in your PIN/password, then press the menu at the top left corner of the app.  Select "System", then "Install click package".  Press the "Pick..." button and select the .click package you want to install.


<br>

### Applications- What doesn't work

Native apps:

- Camera
- Media Player (you can try to choose a music file to play but it doesn't start playing)
- Messaging
- Music- (you can browse music files but they won't play)
- Notes
- Phone (opens but calls don't work)
- UBPorts

Third party apps:

- Converse.js- I enter my login credentials (tried two accounts) but it will never connect
- Feedly- I can log in but the text is so large and I can see the feed index but can't see the list of articles for the feeds.
- uMastonauts (Mastodon client)- can't login to my account
- uNav- crashes on startup
- uWriter- crashes on startup
- Keybase- Following the [install instructions](https://keybase.io/docs/the_app/install_linux) to download and install the .deb package it fails with dependencies that are not available yet.
- WickrMe- There is a [snap package](https://snapcraft.io/wickrme) available, however trying to install snapd fails.


<br>

### Trying to get cellular to work (Not finished yet)

- Run script [enmodem.sh](https://codeberg.org/Supernova/utx) in the Terminal app
- Go to Settings, then Cellular
- Select "2G/3G/4G"
- Select "Carrier & APN"
- Select "APN"
- Select the first (only) listed item
- Use an existing phone and go to it's APN/Access Point Names settings and copy the "Name" and "APN" values into the PinePhone settings.  (Mine were "Verizon" and "VZWINTERNET")
- Select the Check mark at the top right to accept
- Go back one level to "Carrier & APN"
- Select "Carrier"
- It may think for a little bit.  If it finds your carrier select it from the list.
- Trying to send an SMS doesn't work [Gitlab ticket](https://gitlab.com/ubports/community-ports/pinephone/issues/57)
