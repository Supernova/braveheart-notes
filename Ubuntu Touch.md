## Ubuntu Touch

There are two versions of the PinePhone Ubuntu Touch image that I have tested.  The current version I am using has instructions located here:

[https://codeberg.org/Supernova/braveheart-notes/src/branch/master/Ubuntu-Touch-rootfs-pinephone-systemimage.md](https://codeberg.org/Supernova/braveheart-notes/src/branch/master/Ubuntu-Touch-rootfs-pinephone-systemimage.md)

Instructions for the older image are here:

[https://codeberg.org/Supernova/braveheart-notes/src/branch/master/Ubuntu-Touch-rootfs-pinephone.md](https://codeberg.org/Supernova/braveheart-notes/src/branch/master/Ubuntu-Touch-rootfs-pinephone.md)
