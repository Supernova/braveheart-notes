## Sailfish

- Installation

Follow [these instructions](https://wiki.pine64.org/index.php/PinePhone_Software_Release#SailfishOS) to use the flashing script.

- Notes

When first booting up the screen may look black but it is actually just very very dim.  The OS is using automatic brightness adjustment but it causes the screen to be too dark in normal light.  You will need to shine a flashlight into the sensor near the top of the screen for the brightness to increase enough to see the screen.  Once you have booted into the OS adjest the Display settings to turn off atumoatic screen adjustment.

- Good

UI looks great

Has an app store, but selection is limited

- Bad

Browser doesn't work

Are apps being updated?  The ones fro mJolla look like they were last updated in 2017.
